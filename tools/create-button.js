"use strict"

const fs = require('fs');

const draw = require('pureimage');

draw.registerFont('/usr/share/fonts/truetype/freefont/FreeSans.ttf','FreeSans').load(() => {
let img = draw.make(200,100);
let ctx = img.getContext('2d');
ctx.strokeStyle = 'blue';
ctx.lineWidth = 10;
ctx.strokeRect(5, 5, 190, 90);
ctx.fillStyle = '#ffffff';
ctx.font = "64pt 'FreeSans'";
ctx.fillText("Power", 10, 80);
        
draw.encodePNGToStream(img, fs.createWriteStream('out.png')).then(() => {
            console.log("wrote out the png file to out.png");
        }).catch((e)=>{
            console.log("there was an error writing");
        });
});
